
// [S50 ACTIVITY]
import { Button, Row, Col, Card } from 'react-bootstrap';
import {useState, useEffect} from 'react';
// [S50 ACTIVITY END]


// [S50 ACTIVITY]
export default function CourseCard({course}) {

    // Deconstruct the course properties into their own variables
    const {name, description, price} = course;

    /*
    SYNTAX:
        const [getter, setter] = useState(initialGetterValue);
    */
    const [count, setCount] = useState(0);

    // [S51 activity]
    const [slots, setSlots] = useState(30);
    // [S51 activity end]
    

    function enroll(){
        setCount(count + 1)

        // [S51 activity]
        setSlots(slots - 1)
            // if (slots === 0) {
            //     alert("no more seats available");
            //     return;
            // }
        // [S51 activity end]
    }

useEffect(() => {
    if(slots <= 0){
        alert("no more seats available");
    }
}, [slots]);

return (
    <Row className="mt-3 mb-3">
            <Col xs={12}>
                <Card className="cardHighlight p-0">
                    <Card.Body>
                        <Card.Title><h4>{name}</h4></Card.Title>
                        <Card.Subtitle>Description</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price</Card.Subtitle>
                        <Card.Text>PhP {price}</Card.Text>
                        <Card.Subtitle>Count: {count}</Card.Subtitle>
                        <Card.Subtitle>Slots available: {slots}</Card.Subtitle>
                        <Button variant="primary" onClick={enroll}disabled={slots<=0}>Enroll</Button>
                    </Card.Body>
                </Card>
            </Col>
    </Row>        
    )
}
// [S50 ACTIVITY END]



